#include "stm32f7xx.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "console.h"

#define MODIFY_REG_FLD(REG, FLD, VAL) MODIFY_REG((REG), (FLD ## _Msk), ((VAL) << (FLD ## _Pos)))

static void init_usart3(void);
extern void USART3_Handler(void);


static QueueHandle_t usart3_tx_queue;
#define USART3_TX_Q_LEN 32
static QueueHandle_t usart3_rx_queue;
#define USART3_RX_Q_LEN 256
//static char carriage_return = '\r';

unsigned char usart3_rx_overrun_error = 0;

void init_console() {
    init_usart3();
}

static void init_usart3(void) {
    // Enable GPIO_D Clock
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);

    // Select Alternate Mode for PD8 (USART3_TX)
    MODIFY_REG_FLD(GPIOD->MODER, GPIO_MODER_MODER8, 2);

    // Select Alternate Function AF7 for PD8 (USART3_TX)
    MODIFY_REG_FLD(GPIOD->AFR[1], GPIO_AFRH_AFRH0, 7);

    // Select Alternate Mode for PD9 (USART3_RX)
    MODIFY_REG_FLD(GPIOD->MODER, GPIO_MODER_MODER9, 2);

    // Select Alternate Function AF7 for PD9 (USART3_RX)
    MODIFY_REG_FLD(GPIOD->AFR[1], GPIO_AFRH_AFRH1, 7);

    // Select HSI Clk (16 MHz) for USART3
    MODIFY_REG_FLD(RCC->DCKCFGR2, RCC_DCKCFGR2_USART3SEL, 2);

    // USART3 Clk Enable
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN);

    // Baudrate = 16MHz / 16 = 1 Mbs
    WRITE_REG(USART3->BRR, 16);

    usart3_tx_queue = xQueueCreate(USART3_TX_Q_LEN, sizeof(char));
    usart3_rx_queue = xQueueCreate(USART3_RX_Q_LEN, sizeof(char));

    // Enable Interrupts from USART3
    NVIC_SetPriority(USART3_IRQn, 6);
    NVIC_EnableIRQ(USART3_IRQn);

    // 
    // Enable USART, Transmitter, Receiver and RX interrupts 
    WRITE_REG(USART3->CR1,  USART_CR1_UE
                          | USART_CR1_TE 
                          | USART_CR1_RE 
                          | USART_CR1_RXNEIE);
}

int usart3_getchar(int timeout) {
  char c;
  if (usart3_rx_overrun_error) return -1;
  if (xQueueReceive(usart3_rx_queue, &c, timeout)) return (int)c;
  else return -1;  
}

int getchar(void) {
    return usart3_getchar(-1);
}

void usart3_putchar(char c, int timeout) {
  xQueueSendToBack( usart3_tx_queue, &c,  timeout);
  SET_BIT(USART3->CR1, USART_CR1_TXEIE);
}

int putchar(int c) {
  usart3_putchar((char)c, -1);
  //if (c == '\n')
  //  usart3_putchar(c, -1);
  return 0;
}

char *gets(char *str, int n) {
    char *p = str;
    for (int i=0; i<n-1; i++) {
        int c = getchar();
        if (c == -1) return NULL;
        if (c == 4) break;
        *p++ = (char)c;
        if (c == '\n') break;
    }
    *p = 0;
    return str;
}

void USART3_Handler(void) {
  char c, dummy_c;
  if (READ_BIT(USART3->ISR, USART_ISR_TXE)) {
    if (xQueueReceiveFromISR(usart3_tx_queue, &c, NULL)) {
      WRITE_REG(USART3->TDR, (unsigned int) c);
    } else {
      CLEAR_BIT(USART3->CR1, USART_CR1_TXEIE);
    }
  }

  if(READ_BIT(USART3->ISR, USART_ISR_RXNE)) {
    c = READ_REG(USART3->RDR) & 0xff;
    if (xQueueSendToBackFromISR(usart3_rx_queue, &c, NULL) == errQUEUE_FULL) {
      usart3_rx_overrun_error = 1;
      xQueueReceiveFromISR(usart3_rx_queue, &dummy_c, NULL);
      xQueueSendToBackFromISR(usart3_rx_queue, &c, NULL);
    }
  }
}