#include "sysinit.h"
#include "stm32f7xx.h"

uint32_t _estack;
uint32_t _vec_table;
uint32_t _etext;
uint32_t _srelocate;
uint32_t _erelocate;
uint32_t _szero;
uint32_t _ezero;
int main(void);

/* Reset handler */
void Reset_Handler(void);

/* Default empty handler */
void Dummy_Handler(void);

/* Cortex-M handlers */
void *NonMaskableInt_Handler(void)
    __attribute__((weak, alias("Dummy_Handler")));
void *HardFault_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *MemoryManagement_Handler(void)
    __attribute__((weak, alias("Dummy_Handler")));
void *BusFault_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *UsageFault_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SVCall_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DebugMonitor_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *PendSV_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SysTick_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
/* Peripheral handlers */
void *WWDG_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *PVD_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TAMP_STAMP_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *RTC_WKUP_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *FLASH_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *RCC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI4_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream4_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream5_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream6_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *ADC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN1_TX_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN1_RX0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN1_RX1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN1_SCE_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI9_5_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM1_BRK_TIM9_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM1_UP_TIM10_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM1_TRG_COM_TIM11_Handler(void)
    __attribute__((weak, alias("Dummy_Handler")));
void *TIM1_CC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM4_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C1_EV_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C1_ER_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C2_EV_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C2_ER_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPI1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPI2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *USART1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *USART2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *USART3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *EXTI15_10_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *RTC_Alarm_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *OTG_FS_WKUP_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM8_BRK_TIM12_Handler(void)
    __attribute__((weak, alias("Dummy_Handler")));
void *TIM8_UP_TIM13_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM8_TRG_COM_TIM14_Handler(void)
    __attribute__((weak, alias("Dummy_Handler")));
void *TIM8_CC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA1_Stream7_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *FMC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SDMMC1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM5_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPI3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *UART4_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *UART5_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM6_DAC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *TIM7_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream4_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *ETH_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *ETH_WKUP_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN2_TX_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN2_RX0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN2_RX1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN2_SCE_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *OTG_FS_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream5_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream6_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2_Stream7_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *USART6_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C3_EV_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C3_ER_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *OTG_HS_EP1_OUT_Handler(void)
    __attribute__((weak, alias("Dummy_Handler")));
void *OTG_HS_EP1_IN_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *OTG_HS_WKUP_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *OTG_HS_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DCMI_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *RNG_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *FPU_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *UART7_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *UART8_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPI4_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPI5_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPI6_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SAI1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *LTDC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *LTDC_ER_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DMA2D_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SAI2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *QUADSPI_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *LPTIM1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CEC_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C4_EV_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *I2C4_ER_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SPDIF_RX_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DFSDM1_FLT0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DFSDM1_FLT1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DFSDM1_FLT2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *DFSDM1_FLT3_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *SDMMC2_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN3_TX_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN3_RX0_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN3_RX1_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *CAN3_SCE_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *JPEG_Handler(void) __attribute__((weak, alias("Dummy_Handler")));
void *MDIOS_Handler(void) __attribute__((weak, alias("Dummy_Handler")));

/* Exception Table */
__attribute__((section(".vectors"))) const DeviceVectors exception_table = {
    /* Configure Initial Stack Pointer, using linker-generated symbols */
    .pvStack = (void*) (&_estack),

    .pfnReset_Handler = (void (*)(void)) Reset_Handler,
    .pfnNonMaskableInt_Handler = (void (*)(void)) NonMaskableInt_Handler,
    .pfnHardFault_Handler = (void (*)(void)) HardFault_Handler,
    .pfnMemoryManagement_Handler = (void (*)(void)) MemoryManagement_Handler,
    .pfnBusFault_Handler = (void (*)(void)) BusFault_Handler,
    .pfnUsageFault_Handler = (void (*)(void)) UsageFault_Handler,
    .pvReserved_0 = (void *) (0UL), /* Reserved */
    .pvReserved_1 = (void *) (0UL), /* Reserved */
    .pvReserved_2 = (void *) (0UL), /* Reserved */
    .pvReserved_3 = (void *) (0UL), /* Reserved */
    .pfnSVCall_Handler = (void (*)(void)) SVCall_Handler,
    .pfnDebugMonitor_Handler = (void (*)(void)) DebugMonitor_Handler,
    .pvReserved_4 = (void *) (0UL), /* Reserved */
    .pfnPendSV_Handler = (void (*)(void)) PendSV_Handler,
    .pfnSysTick_Handler = (void (*)(void)) SysTick_Handler,

    /* Configurable interrupts */
    .pvWWDG_Handler = (void (*)(void)) WWDG_Handler,
    .pvPVD_Handler = (void (*)(void)) PVD_Handler,
    .pvTAMP_STAMP_Handler = (void (*)(void)) TAMP_STAMP_Handler,
    .pvRTC_WKUP_Handler = (void (*)(void)) RTC_WKUP_Handler,
    .pvFLASH_Handler = (void (*)(void)) FLASH_Handler,
    .pvRCC_Handler = (void (*)(void)) RCC_Handler,
    .pvEXTI0_Handler = (void (*)(void)) EXTI0_Handler,
    .pvEXTI1_Handler = (void (*)(void)) EXTI1_Handler,
    .pvEXTI2_Handler = (void (*)(void)) EXTI2_Handler,
    .pvEXTI3_Handler = (void (*)(void)) EXTI3_Handler,
    .pvEXTI4_Handler = (void (*)(void)) EXTI4_Handler,
    .pvDMA1_Stream0_Handler = (void (*)(void)) DMA1_Stream0_Handler,
    .pvDMA1_Stream1_Handler = (void (*)(void)) DMA1_Stream1_Handler,
    .pvDMA1_Stream2_Handler = (void (*)(void)) DMA1_Stream2_Handler,
    .pvDMA1_Stream3_Handler = (void (*)(void)) DMA1_Stream3_Handler,
    .pvDMA1_Stream4_Handler = (void (*)(void)) DMA1_Stream4_Handler,
    .pvDMA1_Stream5_Handler = (void (*)(void)) DMA1_Stream5_Handler,
    .pvDMA1_Stream6_Handler = (void (*)(void)) DMA1_Stream6_Handler,
    .pvADC_Handler = (void (*)(void)) ADC_Handler,
    .pvCAN1_TX_Handler = (void (*)(void)) CAN1_TX_Handler,
    .pvCAN1_RX0_Handler = (void (*)(void)) CAN1_RX0_Handler,
    .pvCAN1_RX1_Handler = (void (*)(void)) CAN1_RX1_Handler,
    .pvCAN1_SCE_Handler = (void (*)(void)) CAN1_SCE_Handler,
    .pvEXTI9_5_Handler = (void (*)(void)) EXTI9_5_Handler,
    .pvTIM1_BRK_TIM9_Handler = (void (*)(void)) TIM1_BRK_TIM9_Handler,
    .pvTIM1_UP_TIM10_Handler = (void (*)(void)) TIM1_UP_TIM10_Handler,
    .pvTIM1_TRG_COM_TIM11_Handler = (void (*)(void)) TIM1_TRG_COM_TIM11_Handler,
    .pvTIM1_CC_Handler = (void (*)(void)) TIM1_CC_Handler,
    .pvTIM2_Handler = (void (*)(void)) TIM2_Handler,
    .pvTIM3_Handler = (void (*)(void)) TIM3_Handler,
    .pvTIM4_Handler = (void (*)(void)) TIM4_Handler,
    .pvI2C1_EV_Handler = (void (*)(void)) I2C1_EV_Handler,
    .pvI2C1_ER_Handler = (void (*)(void)) I2C1_ER_Handler,
    .pvI2C2_EV_Handler = (void (*)(void)) I2C2_EV_Handler,
    .pvI2C2_ER_Handler = (void (*)(void)) I2C2_ER_Handler,
    .pvSPI1_Handler = (void (*)(void)) SPI1_Handler,
    .pvSPI2_Handler = (void (*)(void)) SPI2_Handler,
    .pvUSART1_Handler = (void (*)(void)) USART1_Handler,
    .pvUSART2_Handler = (void (*)(void)) USART2_Handler,
    .pvUSART3_Handler = (void (*)(void)) USART3_Handler,
    .pvEXTI15_10_Handler = (void (*)(void)) EXTI15_10_Handler,
    .pvRTC_Alarm_Handler = (void (*)(void)) RTC_Alarm_Handler,
    .pvOTG_FS_WKUP_Handler = (void (*)(void)) OTG_FS_WKUP_Handler,
    .pvTIM8_BRK_TIM12_Handler = (void (*)(void)) TIM8_BRK_TIM12_Handler,
    .pvTIM8_UP_TIM13_Handler = (void (*)(void)) TIM8_UP_TIM13_Handler,
    .pvTIM8_TRG_COM_TIM14_Handler = (void (*)(void)) TIM8_TRG_COM_TIM14_Handler,
    .pvTIM8_CC_Handler = (void (*)(void)) TIM8_CC_Handler,
    .pvDMA1_Stream7_Handler = (void (*)(void)) DMA1_Stream7_Handler,
    .pvFMC_Handler = (void (*)(void)) FMC_Handler,
    .pvSDMMC1_Handler = (void (*)(void)) SDMMC1_Handler,
    .pvTIM5_Handler = (void (*)(void)) TIM5_Handler,
    .pvSPI3_Handler = (void (*)(void)) SPI3_Handler,
    .pvUART4_Handler = (void (*)(void)) UART4_Handler,
    .pvUART5_Handler = (void (*)(void)) UART5_Handler,
    .pvTIM6_DAC_Handler = (void (*)(void)) TIM6_DAC_Handler,
    .pvTIM7_Handler = (void (*)(void)) TIM7_Handler,
    .pvDMA2_Stream0_Handler = (void (*)(void)) DMA2_Stream0_Handler,
    .pvDMA2_Stream1_Handler = (void (*)(void)) DMA2_Stream1_Handler,
    .pvDMA2_Stream2_Handler = (void (*)(void)) DMA2_Stream2_Handler,
    .pvDMA2_Stream3_Handler = (void (*)(void)) DMA2_Stream3_Handler,
    .pvDMA2_Stream4_Handler = (void (*)(void)) DMA2_Stream4_Handler,
    .pvETH_Handler = (void (*)(void)) ETH_Handler,
    .pvETH_WKUP_Handler = (void (*)(void)) ETH_WKUP_Handler,
    .pvCAN2_TX_Handler = (void (*)(void)) CAN2_TX_Handler,
    .pvCAN2_RX0_Handler = (void (*)(void)) CAN2_RX0_Handler,
    .pvCAN2_RX1_Handler = (void (*)(void)) CAN2_RX1_Handler,
    .pvCAN2_SCE_Handler = (void (*)(void)) CAN2_SCE_Handler,
    .pvOTG_FS_Handler = (void (*)(void)) OTG_FS_Handler,
    .pvDMA2_Stream5_Handler = (void (*)(void)) DMA2_Stream5_Handler,
    .pvDMA2_Stream6_Handler = (void (*)(void)) DMA2_Stream6_Handler,
    .pvDMA2_Stream7_Handler = (void (*)(void)) DMA2_Stream7_Handler,
    .pvUSART6_Handler = (void (*)(void)) USART6_Handler,
    .pvI2C3_EV_Handler = (void (*)(void)) I2C3_EV_Handler,
    .pvI2C3_ER_Handler = (void (*)(void)) I2C3_ER_Handler,
    .pvOTG_HS_EP1_OUT_Handler = (void (*)(void)) OTG_HS_EP1_OUT_Handler,
    .pvOTG_HS_EP1_IN_Handler = (void (*)(void)) OTG_HS_EP1_IN_Handler,
    .pvOTG_HS_WKUP_Handler = (void (*)(void)) OTG_HS_WKUP_Handler,
    .pvOTG_HS_Handler = (void (*)(void)) OTG_HS_Handler,
    .pvDCMI_Handler = (void (*)(void)) DCMI_Handler,
    .pvReserved_5 = (void *) (0UL), /* Reserved */
    .pvRNG_Handler = (void (*)(void)) RNG_Handler,
    .pvFPU_Handler = (void (*)(void)) FPU_Handler,
    .pvUART7_Handler = (void (*)(void)) UART7_Handler,
    .pvUART8_Handler = (void (*)(void)) UART8_Handler,
    .pvSPI4_Handler = (void (*)(void)) SPI4_Handler,
    .pvSPI5_Handler = (void (*)(void)) SPI5_Handler,
    .pvSPI6_Handler = (void (*)(void)) SPI6_Handler,
    .pvSAI1_Handler = (void (*)(void)) SAI1_Handler,
    .pvLTDC_Handler = (void (*)(void)) LTDC_Handler,
    .pvLTDC_ER_Handler = (void (*)(void)) LTDC_ER_Handler,
    .pvDMA2D_Handler = (void (*)(void)) DMA2D_Handler,
    .pvSAI2_Handler = (void (*)(void)) SAI2_Handler,
    .pvQUADSPI_Handler = (void (*)(void)) QUADSPI_Handler,
    .pvLPTIM1_Handler = (void (*)(void)) LPTIM1_Handler,
    .pvCEC_Handler = (void (*)(void)) CEC_Handler,
    .pvI2C4_EV_Handler = (void (*)(void)) I2C4_EV_Handler,
    .pvI2C4_ER_Handler = (void (*)(void)) I2C4_ER_Handler,
    .pvSPDIF_RX_Handler = (void (*)(void)) SPDIF_RX_Handler,
    .pvReserved_6 = (void *) (0UL), /* Reserved */
    .pvDFSDM1_FLT0_Handler = (void (*)(void)) DFSDM1_FLT0_Handler,
    .pvDFSDM1_FLT1_Handler = (void (*)(void)) DFSDM1_FLT1_Handler,
    .pvDFSDM1_FLT2_Handler = (void (*)(void)) DFSDM1_FLT2_Handler,
    .pvDFSDM1_FLT3_Handler = (void (*)(void)) DFSDM1_FLT3_Handler,
    .pvSDMMC2_Handler = (void (*)(void)) SDMMC2_Handler,
    .pvCAN3_TX_Handler = (void (*)(void)) CAN3_TX_Handler,
    .pvCAN3_RX0_Handler = (void (*)(void)) CAN3_RX0_Handler,
    .pvCAN3_RX1_Handler = (void (*)(void)) CAN3_RX1_Handler,
    .pvCAN3_SCE_Handler = (void (*)(void)) CAN3_SCE_Handler,
    .pvJPEG_Handler = (void (*)(void)) JPEG_Handler,
    .pvMDIOS_Handler = (void (*)(void)) MDIOS_Handler,
};

void __attribute__ ((naked)) Reset_Handler(void) {
  uint32_t *p_src, *p_dest;

  // Init stack pointer, needed for SRAM startup (debug)
  asm volatile("ldr   sp, =_estack");

  /* Set the vector table base address */
  p_src = (uint32_t *)&_vec_table;
  SCB->VTOR = ((uint32_t)p_src & SCB_VTOR_TBLOFF_Msk);

  /* Initialize the relocate segment */
  p_src = &_etext;
  p_dest = &_srelocate;
  if (p_src != p_dest) {
    for (; p_dest < &_erelocate;) {
      *p_dest++ = *p_src++;
    }
  }

  /* Clear the zero segment */
  for (p_dest = &_szero; p_dest < &_ezero;) {
    *p_dest++ = 0;
  }

  /* Initialize the C library */
  //__libc_init_array();

  /* Branch to main function */
  main();

  /* Infinite loop */
  while (1)
    ;
}

void Dummy_Handler(void) {
  while (1) {
  }
}
