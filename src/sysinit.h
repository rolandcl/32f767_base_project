typedef struct _DeviceVectors
{
  /* Stack pointer */
  void *pvStack;
  /* Cortex-M handlers */
  void (*pfnReset_Handler)(void);                        /* -15 Reset Vector, invoked on Power up and warm reset  */
  void (*pfnNonMaskableInt_Handler)(void);               /* -14 Non maskable Interrupt, cannot be stopped or preempted  */
  void (*pfnHardFault_Handler)(void);                    /* -13 Hard Fault, all classes of Fault     */
  void (*pfnMemoryManagement_Handler)(void);             /* -12 Memory Management, MPU mismatch, including Access Violation and No Match  */
  void (*pfnBusFault_Handler)(void);                     /* -11 Bus Fault, Pre-Fetch-, Memory Access Fault, other address/memory related Fault  */
  void (*pfnUsageFault_Handler)(void);                   /* -10 Usage Fault, i.e. Undef Instruction, Illegal State Transition  */
  void *pvReserved_0;
  void *pvReserved_1;
  void *pvReserved_2;
  void *pvReserved_3;
  void (*pfnSVCall_Handler)(void);                       /*  -5 System Service Call via SVC instruction  */
  void (*pfnDebugMonitor_Handler)(void);                 /*  -4 Debug Monitor                        */
  void *pvReserved_4;
  void (*pfnPendSV_Handler)(void);                       /*  -2 Pendable request for system service  */
  void (*pfnSysTick_Handler)(void);                      /*  -1 System Tick Timer                    */

  /* Peripheral handlers */
  void (*pvWWDG_Handler)(void);                   /* Window WatchDog              */
  void (*pvPVD_Handler)(void);                    /* PVD through EXTI Line detection */
  void (*pvTAMP_STAMP_Handler)(void);             /* Tamper and TimeStamps through the EXTI line */
  void (*pvRTC_WKUP_Handler)(void);               /* RTC Wakeup through the EXTI line */
  void (*pvFLASH_Handler)(void);                  /* FLASH                        */
  void (*pvRCC_Handler)(void);                    /* RCC                          */
  void (*pvEXTI0_Handler)(void);                  /* EXTI Line0                   */
  void (*pvEXTI1_Handler)(void);                  /* EXTI Line1                   */
  void (*pvEXTI2_Handler)(void);                  /* EXTI Line2                   */
  void (*pvEXTI3_Handler)(void);                  /* EXTI Line3                   */
  void (*pvEXTI4_Handler)(void);                  /* EXTI Line4                   */
  void (*pvDMA1_Stream0_Handler)(void);           /* DMA1 Stream 0                */
  void (*pvDMA1_Stream1_Handler)(void);           /* DMA1 Stream 1                */
  void (*pvDMA1_Stream2_Handler)(void);           /* DMA1 Stream 2                */
  void (*pvDMA1_Stream3_Handler)(void);           /* DMA1 Stream 3                */
  void (*pvDMA1_Stream4_Handler)(void);           /* DMA1 Stream 4                */
  void (*pvDMA1_Stream5_Handler)(void);           /* DMA1 Stream 5                */
  void (*pvDMA1_Stream6_Handler)(void);           /* DMA1 Stream 6                */
  void (*pvADC_Handler)(void);                    /* ADC1, ADC2 and ADC3s         */
  void (*pvCAN1_TX_Handler)(void);                /* CAN1 TX                      */
  void (*pvCAN1_RX0_Handler)(void);               /* CAN1 RX0                     */
  void (*pvCAN1_RX1_Handler)(void);               /* CAN1 RX1                     */
  void (*pvCAN1_SCE_Handler)(void);               /* CAN1 SCE                     */
  void (*pvEXTI9_5_Handler)(void);                /* External Line[9:5]s          */
  void (*pvTIM1_BRK_TIM9_Handler)(void);          /* TIM1 Break and TIM9          */
  void (*pvTIM1_UP_TIM10_Handler)(void);          /* TIM1 Update and TIM10        */
  void (*pvTIM1_TRG_COM_TIM11_Handler)(void);     /* TIM1 Trigger and Commutation and TIM11 */
  void (*pvTIM1_CC_Handler)(void);                /* TIM1 Capture Compare         */
  void (*pvTIM2_Handler)(void);                   /* TIM2                         */
  void (*pvTIM3_Handler)(void);                   /* TIM3                         */
  void (*pvTIM4_Handler)(void);                   /* TIM4                         */
  void (*pvI2C1_EV_Handler)(void);                /* I2C1 Event                   */
  void (*pvI2C1_ER_Handler)(void);                /* I2C1 Error                   */
  void (*pvI2C2_EV_Handler)(void);                /* I2C2 Event                   */
  void (*pvI2C2_ER_Handler)(void);                /* I2C2 Error                   */
  void (*pvSPI1_Handler)(void);                   /* SPI1                         */
  void (*pvSPI2_Handler)(void);                   /* SPI2                         */
  void (*pvUSART1_Handler)(void);                 /* USART1                       */
  void (*pvUSART2_Handler)(void);                 /* USART2                       */
  void (*pvUSART3_Handler)(void);                 /* USART3                       */
  void (*pvEXTI15_10_Handler)(void);              /* External Line[15:10]s        */
  void (*pvRTC_Alarm_Handler)(void);              /* RTC Alarm (A and B) through EXTI Line */
  void (*pvOTG_FS_WKUP_Handler)(void);            /* USB OTG FS Wakeup through EXTI line */
  void (*pvTIM8_BRK_TIM12_Handler)(void);         /* TIM8 Break and TIM12         */
  void (*pvTIM8_UP_TIM13_Handler)(void);          /* TIM8 Update and TIM13        */
  void (*pvTIM8_TRG_COM_TIM14_Handler)(void);     /* TIM8 Trigger and Commutation and TIM14 */
  void (*pvTIM8_CC_Handler)(void);                /* TIM8 Capture Compare         */
  void (*pvDMA1_Stream7_Handler)(void);           /* DMA1 Stream7                 */
  void (*pvFMC_Handler)(void);                    /* FMC                          */
  void (*pvSDMMC1_Handler)(void);                 /* SDMMC1                       */
  void (*pvTIM5_Handler)(void);                   /* TIM5                         */
  void (*pvSPI3_Handler)(void);                   /* SPI3                         */
  void (*pvUART4_Handler)(void);                  /* UART4                        */
  void (*pvUART5_Handler)(void);                  /* UART5                        */
  void (*pvTIM6_DAC_Handler)(void);               /* TIM6 and DAC1&2 underrun errors */
  void (*pvTIM7_Handler)(void);                   /* TIM7                         */
  void (*pvDMA2_Stream0_Handler)(void);           /* DMA2 Stream 0                */
  void (*pvDMA2_Stream1_Handler)(void);           /* DMA2 Stream 1                */
  void (*pvDMA2_Stream2_Handler)(void);           /* DMA2 Stream 2                */
  void (*pvDMA2_Stream3_Handler)(void);           /* DMA2 Stream 3                */
  void (*pvDMA2_Stream4_Handler)(void);           /* DMA2 Stream 4                */
  void (*pvETH_Handler)(void);                    /* Ethernet                     */
  void (*pvETH_WKUP_Handler)(void);               /* Ethernet Wakeup through EXTI line */
  void (*pvCAN2_TX_Handler)(void);                /* CAN2 TX                      */
  void (*pvCAN2_RX0_Handler)(void);               /* CAN2 RX0                     */
  void (*pvCAN2_RX1_Handler)(void);               /* CAN2 RX1                     */
  void (*pvCAN2_SCE_Handler)(void);               /* CAN2 SCE                     */
  void (*pvOTG_FS_Handler)(void);                 /* USB OTG FS                   */
  void (*pvDMA2_Stream5_Handler)(void);           /* DMA2 Stream 5                */
  void (*pvDMA2_Stream6_Handler)(void);           /* DMA2 Stream 6                */
  void (*pvDMA2_Stream7_Handler)(void);           /* DMA2 Stream 7                */
  void (*pvUSART6_Handler)(void);                 /* USART6                       */
  void (*pvI2C3_EV_Handler)(void);                /* I2C3 event                   */
  void (*pvI2C3_ER_Handler)(void);                /* I2C3 error                   */
  void (*pvOTG_HS_EP1_OUT_Handler)(void);         /* USB OTG HS End Point 1 Out   */
  void (*pvOTG_HS_EP1_IN_Handler)(void);          /* USB OTG HS End Point 1 In    */
  void (*pvOTG_HS_WKUP_Handler)(void);            /* USB OTG HS Wakeup through EXTI */
  void (*pvOTG_HS_Handler)(void);                 /* USB OTG HS                   */
  void (*pvDCMI_Handler)(void);                   /* DCMI                         */
  void *pvReserved_5;                             /* Reserved                     */
  void (*pvRNG_Handler)(void);                    /* RNG                          */
  void (*pvFPU_Handler)(void);                    /* FPU                          */
  void (*pvUART7_Handler)(void);                  /* UART7                        */
  void (*pvUART8_Handler)(void);                  /* UART8                        */
  void (*pvSPI4_Handler)(void);                   /* SPI4                         */
  void (*pvSPI5_Handler)(void);                   /* SPI5                         */
  void (*pvSPI6_Handler)(void);                   /* SPI6                         */
  void (*pvSAI1_Handler)(void);                   /* SAI1                         */
  void (*pvLTDC_Handler)(void);                   /* LTDC                         */
  void (*pvLTDC_ER_Handler)(void);                /* LTDC error                   */
  void (*pvDMA2D_Handler)(void);                  /* DMA2D                        */
  void (*pvSAI2_Handler)(void);                   /* SAI2                         */
  void (*pvQUADSPI_Handler)(void);                /* QUADSPI                      */
  void (*pvLPTIM1_Handler)(void);                 /* LPTIM1                       */
  void (*pvCEC_Handler)(void);                    /* HDMI_CEC                     */
  void (*pvI2C4_EV_Handler)(void);                /* I2C4 Event                   */
  void (*pvI2C4_ER_Handler)(void);                /* I2C4 Error                   */
  void (*pvSPDIF_RX_Handler)(void);               /* SPDIF_RX                     */
  void *pvReserved_6;                             /* Reserved                     */
  void (*pvDFSDM1_FLT0_Handler)(void);            /* DFSDM1 Filter 0 global Interrupt */
  void (*pvDFSDM1_FLT1_Handler)(void);            /* DFSDM1 Filter 1 global Interrupt */
  void (*pvDFSDM1_FLT2_Handler)(void);            /* DFSDM1 Filter 2 global Interrupt */
  void (*pvDFSDM1_FLT3_Handler)(void);            /* DFSDM1 Filter 3 global Interrupt */
  void (*pvSDMMC2_Handler)(void);                 /* SDMMC2                       */
  void (*pvCAN3_TX_Handler)(void);                /* CAN3 TX                      */
  void (*pvCAN3_RX0_Handler)(void);               /* CAN3 RX0                     */
  void (*pvCAN3_RX1_Handler)(void);               /* CAN3 RX1                     */
  void (*pvCAN3_SCE_Handler)(void);               /* CAN3 SCE                     */
  void (*pvJPEG_Handler)(void);                   /* JPEG                         */
  void (*pvMDIOS_Handler)(void);                  /* MDIOS                        */
} DeviceVectors;
