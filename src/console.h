#ifndef INC_CONSOLE_H
#define INC_CONSOLE_H

void init_console(void);

int putchar(int c);
int getchar(void);
void usart3_putchar(char c, int timeout);
int usart3_getchar(int timeout);
unsigned char usart3_rx_overrun_error;
char *gets(char *str, int n);

#endif