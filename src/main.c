#include "FreeRTOS.h"
#include "console.h"
#include "opt/printf-stdarg.h"
#include "opt/scanf.h"
#include "queue.h"
#include "stm32f7xx.h"
#include "task.h"
#include <string.h>

#define MODIFY_REG_FLD(REG, FLD, VAL)                                          \
  MODIFY_REG((REG), (FLD##_Msk), ((VAL) << (FLD##_Pos)))

extern char _sheap;
extern char _estack;

uint32_t SystemCoreClock = 16000000; // Modified in init_rcc()

void init_rcc(void);
void init_freertos(void);
void start_tasks(void);
void blinking_led2_task(void *pvParameters);
void blinking_led3_task(void *pvParameters);
void shell_task(void *pvParameters);
void status_task(void *pvParameters);
void HardFault_Handler(void);

int main(void) {
  // Enable Caches
  SCB_InvalidateICache();
  SCB_InvalidateDCache();
  SCB_EnableICache();
  SCB_EnableDCache();

  init_rcc();
  init_freertos();
  init_console();
  start_tasks();
  while (1) {
  }
}

void init_rcc(void) {
  // !!! Procedure from Reference Manual RM0410 page 125

  // HSE set to external clock (8MHz on NucleoBoard)
  SET_BIT(RCC->CR, RCC_CR_HSEBYP);
  SET_BIT(RCC->CR, RCC_CR_HSEON);

  // Wait until HSE is Ready
  while (!READ_BIT(RCC->CR, RCC_CR_HSERDY))
    ;

  // Configure PLL
  // PLL clk = HSE_Clk * pllN / pllM / pllP
  // 8MHz * 90 / 2 / 2 = 180MHz
  MODIFY_REG_FLD(RCC->PLLCFGR, RCC_PLLCFGR_PLLN, 90);
  MODIFY_REG_FLD(RCC->PLLCFGR, RCC_PLLCFGR_PLLM, 2);
  MODIFY_REG_FLD(RCC->PLLCFGR, RCC_PLLCFGR_PLLP, 0); // 0 => / 2

  // PLL Source is HSE
  SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLLSRC);

  // PLL Enable
  SET_BIT(RCC->CR, RCC_CR_PLLON);

  // Enable Overdrive Mode
  // SET_BIT(PWR->CR1, PWR_CR1_ODEN);
  // Wait until Overdrive Mode is Ready
  // while (! READ_BIT(PWR->CSR1, PWR_CSR1_ODRDY));

  // Switch Voltage Regulator to Overdrive Mode
  // SET_BIT(PWR->CR1, PWR_CR1_ODSWEN);
  // Wait until Overdrive Mode is Active
  // while (! READ_BIT(PWR->CSR1, PWR_CSR1_ODSWRDY));

  // Set Flash Latency to 6 Wait States
  MODIFY_REG_FLD(FLASH->ACR, FLASH_ACR_LATENCY, 6);

  // Wait until PLL is Ready
  while (!READ_BIT(RCC->CR, RCC_CR_PLLRDY))
    ;

  // System Clk source is PLL
  MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_PLL);

  SystemCoreClock = 180000000;
}

void init_freertos(void) {
  // Initialise Heap 5 before using any kernel objects
  HeapRegion_t xHeapRegions[] = {
      {(unsigned char *)&_sheap,
       ((const char *)&_estack - (const char *)&_sheap -
        configMINIMAL_STACK_SIZE)},
      {NULL, 0} /* Marks the end of the array. */};
  vPortDefineHeapRegions(xHeapRegions);
}

void start_tasks(void) {
  xTaskCreate(blinking_led2_task, "blinking_led2", 64, NULL, 1, NULL);

  xTaskCreate(blinking_led3_task, "blinking_led3", 64, NULL, 1, NULL);

  xTaskCreate(shell_task, "shell", 512, NULL, 1, NULL);

  /*
  xTaskCreate(
            motor_ihm01a1_task,
            "motor_ihm01a1",
            256,
            NULL,
            1,
            NULL);
  */

  vTaskStartScheduler();
}

void blinking_led2_task(void *pvParameters) {
  // Enable GPIO_B Clock
  SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);

  // wait for 2 pclock before accessing the peripheral registers.
  __NOP();
  __NOP();
  __NOP();

  // PB7 set to Ouptut Mode
  MODIFY_REG_FLD(GPIOB->MODER, GPIO_MODER_MODER7, 1);

  // The loop...
  while (1) {
    WRITE_REG(GPIOB->BSRR, GPIO_BSRR_BS_7);
    vTaskDelay(500);
    WRITE_REG(GPIOB->BSRR, GPIO_BSRR_BR_7);
    vTaskDelay(1500);
  }
}

void blinking_led3_task(void *pvParameters) {
  // Enable GPIO_B Clock
  SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);

  // wait for 2 pclock before accessing the peripheral registers.
  __NOP();
  __NOP();
  __NOP();

  // PB14 set to Ouptut Mode
  MODIFY_REG_FLD(GPIOB->MODER, GPIO_MODER_MODER14, 1);

  // The loop...
  while (1) {
    WRITE_REG(GPIOB->BSRR, GPIO_BSRR_BS_14);
    vTaskDelay(1000);
    WRITE_REG(GPIOB->BSRR, GPIO_BSRR_BR_14);
    vTaskDelay(1000);
  }
}

void status_task(void *pvParameters) {
  char *buf = pvPortMalloc(1024);
  configASSERT(buf);

  while (1) {
    vTaskList(buf);
    printf("\n%s\n", buf);

    int free_heap_size = xPortGetFreeHeapSize();
    printf("Free heap size : %d\n", free_heap_size);

    int c;
    while ((c = getchar()) != -1)
      printf("%c", c);
    printf("\n");
    vTaskDelay(1000);
  }
}

#define SHELL_BUF_SIZE 1024

void shell_task(void *pvParameters) {
  char *buf = pvPortMalloc(SHELL_BUF_SIZE);
  char error = ' ';
  int speed;

  configASSERT(buf);
  printf("\n");
  while (1) {
    printf("%c > ", error);
    error = ' ';
    gets(buf, SHELL_BUF_SIZE);
    strrstrip(buf, SHELL_BUF_SIZE);

    if (strlen(buf) == 0);
    else if (!strcmp("tasks", buf)) {
      vTaskList(buf);
      printf("\n%s\n", buf);
    }
    else {
      error = 'E';
    }
  }
}

// For debugging
void HardFault_Handler(void) {
  volatile int hf_status = SCB->HFSR;
  while (1) {
  }
}
