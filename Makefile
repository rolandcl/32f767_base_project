V = 1
BIN = 32f767_base_project

STM32F7xx_CMSIS_DIR = /home/rolandcl/Projects/EmbeddedSystems/cmsis_packs/Keil.STM32F7xx_DFP.2.12.0/Drivers/CMSIS/Device/ST/STM32F7xx
CORTEX_CMSIS_DIR = /home/rolandcl/Projects/EmbeddedSystems/cmsis_packs/ARM.CMSIS.5.6.0/CMSIS

INC = -I$(STM32F7xx_CMSIS_DIR)/Include -I$(CORTEX_CMSIS_DIR)/Include
SRC_C += $(wildcard src/*.c)
SRC_LD = link.ld

# FreeRTOS
FREERTOS_DIR = /home/rolandcl/Projects/EmbeddedSystems/FreeRTOSv10.2.1/FreeRTOS/Source
INC += -I$(FREERTOS_DIR)/include -I$(FREERTOS_DIR)/portable/GCC/ARM_CM7/r0p1
SRC_C += $(wildcard $(FREERTOS_DIR)/*.c)
SRC_C += $(FREERTOS_DIR)/portable/MemMang/heap_5.c
SRC_C += $(FREERTOS_DIR)/portable/GCC/ARM_CM7/r0p1/port.c
SRC_C += src/opt/printf-stdarg.c
SRC_C += src/opt/scanf.c

ARCHFLAGS = -mcpu=cortex-m7
FPFLAGS = -mfloat-abi=softfp -mfpu=fpv5-d16
OPTFLAGS = -Og
DBGFLAGS = -ggdb
# Use newlib-nano, include syscalls stubs (nosys)
SPECSFLAGS = --specs=nano.specs --specs=nosys.specs -fno-builtin-printf

DEF = -DSTM32F767xx

# Tools selection
CROSS_COMPILE = arm-none-eabi-

BUILD_DIR = ./build

OPENOCD = openocd -f interface/stlink-v2-1.cfg -f target/stm32f7x.cfg

###############################################################################################
CC := $(CROSS_COMPILE)gcc
CXX := $(CROSS_COMPILE)g++
AS := $(CROSS_COMPILE)gcc
NM := $(CROSS_COMPILE)nm
OBJCOPY := $(CROSS_COMPILE)objcopy
OBJDUMP := $(CROSS_COMPILE)objdump
SIZE := $(CROSS_COMPILE)size
GDB := $(CROSS_COMPILE)gdb
LD := $(CROSS_COMPILE)gcc

# Include directories
INC += -I.

# Compiler and linker flags
ARCHFLAGS += -mthumb -mabi=aapcs $(FPFLAGS)
WARNFLAGS = -Wall -Wextra -Wundef -Wshadow -Wimplicit-function-declaration \
             -Wredundant-decls -Wstrict-prototypes -Wmissing-prototypes \
             -Wconversion -Wno-sign-conversion -Wdouble-promotion \
			 -Wfloat-conversion -pedantic
PREPFLAGS = -MD -MP

# This should be only enabled during development:
#WARNFLAGS += -Werror

CFLAGS = $(ARCHFLAGS) $(OPTFLAGS) $(DBGFLAGS) $(WARNFLAGS) $(PREPFLAGS) \
         -std=gnu99 \
         -ffunction-sections -fdata-sections -fno-strict-aliasing \
         $(SPECSFLAGS)

LDFLAGS = $(ARCHFLAGS) $(OPTFLAGS) $(DBGFLAGS) \
          -Wl,-Map=$(BUILD_DIR)/$(BIN).map -Wl,--gc-sections \
          $(SPECSFLAGS)

FILENAMES_C = $(notdir $(SRC_C))
OBJS_C = $(addprefix $(BUILD_DIR)/, $(FILENAMES_C:.c=.o))
vpath %.c $(dir $(SRC_C))

OUTPUTS = $(BUILD_DIR)/$(BIN).elf
OUTPUTS += $(BUILD_DIR)/$(BIN).openocd.gdb

ifeq ($(V), 0)
	CMD_ECHO = @
else
	CMD_ECHO =
endif

OPENOCD_GDBINIT = \
        target remote localhost:3333\n\
        set mem inaccessible-by-default off\n\
        set "$$"pc = Reset_Handler

###############################################################################################
.PHONY: default
default: all

.PHONY: all
all: $(BUILD_DIR) $(OUTPUTS)
	@echo ""
	$(CMD_ECHO) $(SIZE) $(BUILD_DIR)/$(BIN).elf

$(BUILD_DIR):
	$(CMD_ECHO) mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/%.o: %.c
	@echo "  CC      $(notdir $@)"
	$(CMD_ECHO) $(CC) $(CFLAGS) $(DEF) $(INC) -c -o $@ $<

$(BUILD_DIR)/$(BIN).elf: $(OBJS_ASM) $(OBJS_C) $(OBJS_CXX) | $(SRC_LD)
	@echo "  LD      $(notdir $@)"
	$(CMD_ECHO) $(LD) $^ $(LDFLAGS) $(INC) -T$(SRC_LD) -o $@

$(BUILD_DIR)/$(BIN).openocd.gdb:
	@echo "  ECHO    $(notdir $@)"
	$(CMD_ECHO) echo "$(OPENOCD_GDBINIT)" > $@

.PHONY: gdb_ram
gdb_ram: $(BUILD_DIR)/$(BIN).elf
	$(CMD_ECHO) $(OPENOCD) -c \
	"init; \
	reset halt; \
	sleep 100; \
	load_image $^ 0"

.PHONY: gdb_flash
gdb_flash: $(BUILD_DIR)/$(BIN).elf
	$(CMD_ECHO) $(OPENOCD) -c \
	"init; \
	reset halt; \
	sleep 100; \
	flash write_image erase $^; \
	reset halt"

.PHONY: flash
flash: $(BUILD_DIR)/$(BIN).elf
	$(CMD_ECHO) $(OPENOCD) -c \
	"init; \
	reset halt; \
	sleep 100; \
	flash write_image erase $^; \
	reset run; \
	shutdown"

.PHONY: run_ram
run_ram: $(BUILD_DIR)/$(BIN).elf
	$(CMD_ECHO) $(OPENOCD) -c \
	"init; \
	reset halt; \
	sleep 100; \
	load_image $^ 0; \
	resume 0x200001f8; \
	shutdown"

.PHONY: debug
debug: $(BUILD_DIR)/$(BIN).elf | $(BUILD_DIR)/$(BIN).openocd.gdb
	$(CMD_ECHO) $(GDB) -tui -x $| $^

.PHONY: clean
clean:
	rm -f $(OUTPUTS)
	rm -f $(BUILD_DIR)/$(BIN).map $(BUILD_DIR)/*.o
